{-# LANGUAGE AllowAmbiguousTypes       #-}
{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DefaultSignatures         #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE KindSignatures            #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeOperators             #-}
module Lib where

import           GHC.Generics
import           GHC.TypeLits    (KnownSymbol)
import           Type.Reflection

  {- UNIONS -}

-- | Class for checking whether a type is a sum type.
class IsUnion a where
  isUnion :: Bool
  default isUnion :: (Generic a, GIsUnion (Rep a)) => Bool
  isUnion = gIsUnion @(Rep a)

class GIsUnion (f :: * -> *) where
  gIsUnion :: Bool

-- Summary of all these types
-- https://hackage.haskell.org/package/base-4.17.0.0/docs/GHC-Generics.html#g:5

instance (GIsUnion f) => GIsUnion (M1 D c f) where
  gIsUnion = gIsUnion @f

instance GIsUnion (f :+: g) where
  gIsUnion = True

instance GIsUnion (f :*: g) where
  gIsUnion = False

instance GIsUnion (M1 S c f) where
  gIsUnion = False

instance GIsUnion (M1 C c f) where
  gIsUnion = False

instance GIsUnion V1 where
  gIsUnion = False
instance GIsUnion U1 where
  gIsUnion = False

-- | Marker class, for types that are sum types. Interface here is that you get
-- a compile-time check of whether the type is a union. If `instance MarkUnion
-- t` fails with a "no instance for" compiler error, then `t` is not a union.
class (Generic a, GMarkUnion (Rep a)) => MarkUnion a

data Union
  = forall a. (MarkUnion a) => Union a

-- | Container for sum types.
type UnionList = [Union]

class GMarkUnion (f :: * -> *)

instance (GMarkUnion f) => GMarkUnion (M1 D c f)

instance GMarkUnion (f :+: g)

  {- RECORDS -}

-- | Is the type a record, at the top level?
class IsRecord a where
  isRecord :: Bool
  default isRecord :: (Generic a, GIsRecord (Rep a)) => Bool
  isRecord = gIsRecord @(Rep a)

class GIsRecord (f :: * -> *) where
  gIsRecord :: Bool

instance GIsRecord V1 where
  gIsRecord = False

instance GIsRecord U1 where
  gIsRecord = False

instance (GIsRecord a, GIsRecord b) => GIsRecord (a :*: b) where
  gIsRecord = gIsRecord @a && gIsRecord @b

instance (GIsRecord a, GIsRecord b) => GIsRecord (a :+: b) where
  gIsRecord = False

-- Record constructors
instance GIsRecord (M1 S ('MetaSel ('Just sel) u ss l) f) where
  gIsRecord = True

-- Product type
instance GIsRecord (M1 S ('MetaSel 'Nothing u ss l) f) where
  gIsRecord = False

instance (GIsRecord f) => GIsRecord (M1 C c f) where
  gIsRecord = gIsRecord @f

instance (GIsRecord f) => GIsRecord (M1 D c f) where
  gIsRecord = gIsRecord @f


-- | Cleaner way to do IsRecord, where you have a separate class for GIsRecordSel'
-- Is the type a record, at the top level?
class IsRecord' a where
  isRecord' :: Bool
  default isRecord' :: (Generic a, GIsRecord' (Rep a)) => Bool
  isRecord' = gIsRecord' @(Rep a)

class GIsRecord' (f :: * -> *) where
  gIsRecord' :: Bool

instance GIsRecord' V1 where
  gIsRecord' = False

instance GIsRecord' U1 where
  gIsRecord' = False

instance GIsRecord' (Rec0 f) where
  gIsRecord' = False

-- NOTE: key here is to use gIsRecordSel' on
-- selector/constructor types, not gIsRecord'
instance (GIsRecordSel' a, GIsRecord' b, GIsRecordSel' b) => GIsRecord' (a :*: b) where
  -- Records with more than two fields are nested in the right-hand element.
  -- Need to check that either the RHS is a record selector OR is a record.
  gIsRecord' = gIsRecordSel' @a && (gIsRecord' @b || gIsRecordSel' @b)

instance (GIsRecord' a, GIsRecord' b) => GIsRecord' (a :+: b) where
  gIsRecord' = False

-- NOTE this is not a record. It might be a record selector, which is checked in gIsRecordSel'.
instance (GIsRecord' f) => GIsRecord' (M1 S c f) where
  gIsRecord' = False

instance (GIsRecord' f) => GIsRecord' (M1 C c f) where
  gIsRecord' = gIsRecord' @f

instance (GIsRecord' f) => GIsRecord' (M1 D c f) where
  gIsRecord' = gIsRecord' @f

-- | Is this a named selector?
class GIsRecordSel' (f :: * -> *) where
  gIsRecordSel' :: Bool

-- The only important instances
-- Record selector
instance GIsRecordSel' (S1 ('MetaSel ('Just sel) su ss ds) f) where
  gIsRecordSel' = True

-- Product selector
instance GIsRecordSel' (S1 ('MetaSel 'Nothing su ss ds) f) where
  gIsRecordSel' = False

-- Overhead. Do not recurse through f.
instance GIsRecordSel' (D1 c f) where
  gIsRecordSel' = False

instance GIsRecordSel' (C1 c f) where
  gIsRecordSel' = False

instance GIsRecordSel' V1 where
  gIsRecordSel' = False

instance GIsRecordSel' U1 where
  gIsRecordSel' = False

instance GIsRecordSel' (Rec0 f) where
  gIsRecordSel' = False

instance GIsRecordSel' (a :*: b) where
  gIsRecordSel' = False

instance (GIsRecord a, GIsRecord b) => GIsRecordSel' (a :+: b) where
  gIsRecordSel' = False

  {- Display SHAPE -}

-- | Show 'Rep a'.
showRep :: forall a. (Typeable (Rep a), Generic a) => String
showRep = show (typeRep @(Rep a))

-- TODO it would be nice to do this with singletons, as Selectors does.

-- | Provide a list of top-level selectors and contained values for record
-- types, as key-value string pairs. The values give the t in 'Rec0 t' within
-- the generic representation. Returns an empty list for all other types,
-- including products with no selectors. Note the difference (in addition to
-- the types on which they are defined) with the 'selName' methods
-- 'Data.Generics.Selectors', which returns '""' for product type selectors.
-- 'selectorsOf' does not count those as selectors.
class SelectorsOf a where
  selectorsOf :: [(String, String)]
  default selectorsOf :: (Generic a, GSelectorsOf (Rep a)) => [(String, String)]
  selectorsOf = gSelectorsOf @(Rep a)

class GSelectorsOf (f :: * -> *) where
  gSelectorsOf :: [(String, String)]

-- The relevant instances. sel is a Symbol. Rec0 should be the only f.
instance (Typeable a, KnownSymbol sel) =>
  GSelectorsOf (S1 ('MetaSel ('Just sel) su ss ds) (Rec0 a)) where
  gSelectorsOf = [(show $ typeRep @sel, show $ typeRep @a)]

instance GSelectorsOf (S1 ('MetaSel 'Nothing su ss ds) f) where
  gSelectorsOf = []

instance (GSelectorsOf f, GSelectorsOf g) => GSelectorsOf (f :*: g) where
  gSelectorsOf = gSelectorsOf @f ++ gSelectorsOf @g

instance (GSelectorsOf f) => GSelectorsOf (D1 c f) where
  gSelectorsOf = gSelectorsOf @f

instance (GSelectorsOf f) => GSelectorsOf (C1 c f) where
  gSelectorsOf = gSelectorsOf @f

-- Boilerplate
instance (GSelectorsOf f, GSelectorsOf g) => GSelectorsOf (f :+: g) where
  gSelectorsOf = []

instance GSelectorsOf (K1 c f) where
  gSelectorsOf = []

instance GSelectorsOf U1 where
  gSelectorsOf = []

instance GSelectorsOf V1 where
  gSelectorsOf = []
