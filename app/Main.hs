{-# LANGUAGE DeriveGeneric    #-}
{-# LANGUAGE TypeApplications #-}
module Main where

import           Data.Map        (Map)
import           GHC.Generics
import           Lib
import           Type.Reflection (Typeable)

data MyNuttin deriving (Generic)

data MyMarker = MkMyMarker deriving (Generic)

data MyUnion
  = Yes
  | No Int
  deriving (Generic)

data MyUnionA a
  = YesA
  | NoA a
  deriving (Generic)

data MyUnionRecurse
  = YesR
  | NoR MyUnionRecurse
  deriving (Generic)

data MyRec a
  = MkMyRec
      { rec1 :: a
      , rec2 :: a
      , rec3 :: a
      }
  deriving (Generic)

data MyProdRec a
  = MkProdRec a (MyRec a)
  deriving (Generic)

data MyMap
  = Map1 (Map String String)
  | NoMap
  deriving (Generic)

instance IsUnion MyNuttin
instance IsUnion MyMarker
instance IsUnion MyUnion
instance IsUnion (MyUnionA a)
instance IsUnion MyUnionRecurse
instance IsUnion (MyRec a)
instance IsUnion (MyProdRec a)
instance IsUnion MyMap

instance IsRecord MyNuttin
instance IsRecord MyMarker
instance IsRecord MyUnion
instance IsRecord (MyUnionA a)
instance IsRecord MyUnionRecurse
instance IsRecord (MyRec a)
instance IsRecord (MyProdRec a)
instance IsRecord MyMap

instance IsRecord' MyNuttin
instance IsRecord' MyMarker
instance IsRecord' MyUnion
instance IsRecord' (MyUnionA a)
instance IsRecord' MyUnionRecurse
instance IsRecord' (MyRec a)
instance IsRecord' (MyProdRec a)
instance IsRecord' MyMap

instance MarkUnion MyUnion
instance MarkUnion MyUnionRecurse
-- Doesn't compile because of superclass constraint.
--instance MarkUnion (MyRec a)

-- | Inhomogeneous list for unions only.
uList :: UnionList
uList = [Union YesR, Union Yes]

instance SelectorsOf MyNuttin
instance SelectorsOf MyMarker
instance SelectorsOf MyUnion
instance SelectorsOf (MyUnionA a)
instance SelectorsOf MyUnionRecurse
-- Note reqirement here, because Rep a actually reaches all the way.
instance (Typeable a) => SelectorsOf (MyRec a)
instance SelectorsOf (MyProdRec a)


main :: IO ()
main = do
  -- IsUnion
  putStrLn "IsUnion"
  putStrLn "=========="
  putStrLn "Is MyNuttin a Union?"
  print (isUnion @MyNuttin)
  putStrLn "Is MyMarker a Union?"
  print (isUnion @MyMarker)
  putStrLn "Is MyUnion a Union?"
  print (isUnion @MyUnion)
  putStrLn "Is (MyUnionA Int) a Union?"
  print (isUnion @(MyUnionA Int))
  putStrLn "Is MyUnionRecurse a Union?"
  print (isUnion @MyUnionRecurse)
  putStrLn "Is (MyRec Int) a Union?"
  print (isUnion @(MyRec Int))
  putStrLn "Is (MyRec MyUnion) a Union?"
  print (isUnion @(MyRec MyUnion))
  putStrLn "Is MyMap a Union?"
  print (isUnion @MyMap)
  -- IsRecord
  putStrLn "==========="
  putStrLn "IsRecord"
  putStrLn "==========="
  putStrLn "Is MyNuttin a Record?"
  print (isRecord @MyNuttin)
  putStrLn "Is MyMarker a Record?"
  print (isRecord @MyMarker)
  putStrLn "Is MyUnion a Record?"
  print (isRecord @MyUnion)
  putStrLn "Is (MyUnionA Int) a Record?"
  print (isRecord @(MyUnionA Int))
  putStrLn "Is (MyUnionA (MyRec Int)) a Record?"
  print (isRecord @(MyUnionA (MyRec Int)))
  putStrLn "Is MyUnionRecurse a Record?"
  print (isRecord @MyUnionRecurse)
  putStrLn "Is (MyRec Int) a Record?"
  print (isRecord @(MyRec Int))
  putStrLn "Is (MyRec (MyRec Int)) a Record?"
  print (isRecord @(MyRec (MyRec Int)))
  putStrLn "Is (MyRec MyUnion) a Record?"
  print (isRecord @(MyRec MyUnion))
  putStrLn "Is (MyProdRec Int) a Record?"
  print (isRecord @(MyProdRec Int))
  putStrLn "Is MyMap a Record?"
  print (isRecord @MyMap)
  -- IsRecord'
  putStrLn "IsRecord'"
  putStrLn "==========="
  putStrLn "Is MyNuttin a Record?"
  print (isRecord' @MyNuttin)
  putStrLn "Is MyMarker a Record?"
  print (isRecord' @MyMarker)
  putStrLn "Is MyUnion a Record?"
  print (isRecord' @MyUnion)
  putStrLn "Is (MyUnionA Int) a Record?"
  print (isRecord' @(MyUnionA Int))
  putStrLn "Is (MyUnionA (MyRec Int)) a Record?"
  print (isRecord' @(MyUnionA (MyRec Int)))
  putStrLn "Is MyUnionRecurse a Record?"
  print (isRecord' @MyUnionRecurse)
  putStrLn "Is (MyRec Int) a Record?"
  print (isRecord' @(MyRec Int))
  putStrLn "Is (MyRec (MyRec Int)) a Record?"
  print (isRecord' @(MyRec (MyRec Int)))
  putStrLn "Is (MyRec MyUnion) a Record?"
  print (isRecord' @(MyRec MyUnion))
  putStrLn "Is (MyProdRec Int) a Record?"
  print (isRecord' @(MyProdRec Int))
  -- SelectorsOf
  putStrLn "SelectorsOf"
  putStrLn "==========="
  putStrLn "Selectors of MyNuttin"
  print (selectorsOf @MyNuttin)
  putStrLn "Selectors of MyUnion"
  print (selectorsOf @MyUnion)
  putStrLn "Selectors of MyUnionA (MyRec Int)"
  putStrLn "Should be empty since MyUnionA is not a record."
  print (selectorsOf @(MyUnionA (MyRec Int)))
  putStrLn "Selectors of MyRec Int"
  print (selectorsOf @(MyRec Int))
  putStrLn "Selectors of (MyRec (MyRec Int))"
  print (selectorsOf @(MyRec (MyRec Int)))
